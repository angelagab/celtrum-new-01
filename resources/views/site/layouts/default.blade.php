<!DOCTYPE html>
<html class="no-js" lang="en">

	@include('site/layouts.head')

	<body>

	    @yield('content')

	   	@yield('styles') 	

	   	@yield('scripts')

   	</body>

</html>  