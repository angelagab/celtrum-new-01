<head>
<title>Celtrum</title>
<meta charset="UTF-8"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- description -->
    <meta name="description" content="We have created a new blockchain, a network of smart mining contracts that will revolutionize the way we relate ore to cryptocurrencies.">
    <!-- keywords -->
    <meta name="keywords" content="Celtrum, Currency, ICO, Extraction, Ore, Blockchain, Economy, Investment, Celscan, Gold, Golden, Golden Place, Mining, Crypto">
    <link rel="shortcut icon" href="assets/img/celtrum_favicon.png">
    <link href="assets/fonts/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="assets/fonts/elegant-fonts.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="assets/css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="assets/css/trackpad-scroll-emulator.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="/theme-assets/fonts/flag-icon-css/css/flag-icon.min.css">

    <title>Soon</title>
    </head>

@yield('styles')


@section('scripts')

       <!-- javascript libraries -->

<script type="text/javascript" src="assets/js/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js"></script>
<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.trackpad-scroll-emulator.min.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>
<link href="https://fonts.googleapis.com/css2?family=Fira+Sans:wght@400;700;900&display=swap" rel="stylesheet">
       
    <script type="text/javascript">
    var latitude = 34.038405;
    var longitude = -117.946944;
    var markerImage = "assets/img/map-marker-w.png";
    var mapTheme = "dark";
    var mapElement = "map-contact";
    google.maps.event.addDomListener(window, 'load', simpleMap(latitude, longitude, markerImage, mapTheme, mapElement));
</script>
  

@stop

