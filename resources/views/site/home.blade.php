
@extends('site.layouts.default')

@section('title')
@parent
@stop

@section('content')

<body class=" frame">

<div id="outer-wrapper" class="animate translate-z-in">
    <div id="inner-wrapper">
        <div id="table-wrapper">
            <div class="container">
                <div id="row-header">
                    <div class="col-md-10">
                <header><a href="/" id="brand" class="animate animate fade-in animation-time-3s"><img src="assets/img/icon-celtrum-white.png" style="margin-bottom: 30px; width: 150px; height: auto;"></a></header></div>
                    </div>
                    <div class="col-md-2">
                        <!-- LANGUANGE SELECTOR -->
                        <div class="btn-group" style="margin-top: 20px; margin-left: 40px">
                            <a type="button" class="btn btn-default btn-lang" title="@lang('site/layouts/home.lang')"><img src="@lang('site/home.lang_img')" style="max-height: 10px;width:15px;"> @lang('site/home.lang')</a>
                                <button type="button" class="btn btn-lang dropdown-toggle" data-toggle="dropdown" aria-expanded="false" title="@lang('site/home.selector')" style="background-color: #C06550; box-shadow: none;">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{URL::to('/changeLocale/es')}}" title="Español"><img src="/assets/flags/es.png" style="max-height: 10px;width:15px;"> ES</a>
                                </li>
                                <li>
                                    <a href="{{URL::to('/changeLocale/en')}}" title="English"><img src="/assets/flags/en.png" style="max-height: 10px;width:15px;"> EN</a>
                                </li>
                                <li>
                                    <a href="{{URL::to('/changeLocale/pt')}}" title="Português"><img src="/assets/flags/pt_br.png" style="max-height: 10px;width:15px;"> PT-BR</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                <!--end row-header-->
                <div id="row-content" style="margin-top: 50px;">
                    <div id="content-wrapper">
                        <div class="row vertical-aligned-wrapper"    style="margin-top: 50px;">
                            <div class="col-md-8 col-sm-8 vertical-aligned-element">
                                <div id="content" class="animate translate-z-out animation-time-2s delay-05s">
                                    <h2 class="opacity-70">@lang('site/home.home1')</h2>
                                    <h1>@lang('site/home.home2')</h1>
                                    <div class="row">
                                        <div class="col-md-8 col-sm-8">
                                            <h3>@lang('site/home.home3')</h3><p>@lang('site/home.home4')
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!--end content-->
                            </div>
                            <div class="col-md-4 col-sm-4 vertical-aligned-element">
                                <form method="post" class="form clearfix has-background animate translate-z-in animation-time-2s delay-03s">
                                    <h2>@lang('site/home.home5')</h2>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="name" placeholder="@lang('site/home.home6')" required>
                                    </div>
                                    <!--end form-group -->
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="email" placeholder="@lang('site/home.home7')" required>
                                    </div>
                                    <!--end form-group -->
                                    <div class="form-group clearfix">
                                        <button type="submit" class="btn pull-right btn-default btn-framed btn-rounded" id="form-contact-submit">@lang('site/home.home8')</button>
                                    </div>
                                    <!--end form-group -->
                                    <div class="form-contact-status"></div>
                                </form>
                                <!--end form-contact -->
                            </div>
                        </div>
                    </div>
                    <!--end content-wrapper-->
                </div>
                <!--end row-content-->
                <div id="row-footer">
                    <footer>
                        <div class="social-icons">
                            <a href="https://www.instagram.com/celtrum.io" class="animate fade-in animation-time-1s delay-08s"><i class="fa fa-facebook"></i></a>
                            <a href="https://www.facebook.com/celtrum" class="animate fade-in animation-time-1s delay-06s"><i class="fa fa-instagram"></i></a>
                        </div>
                    </footer>
                </div>
                <!--end row-footer-->
            </div>
            <!--end container-->
        </div>
        <!--end table-wrapper-->
        <div class="background-wrapper has-vignette">
            <div class="bg-transfer opacity-80"><img src="assets/img/background-06.png" alt=""></div>
        </div>
        <!--end background-wrapper-->
    </div>
    <!--end inner-wrapper-->
</div>
<!--end outer-wrapper-->



<div class="backdrop"></div>


</body>

<div class="backdrop"></div>

        @endsection

