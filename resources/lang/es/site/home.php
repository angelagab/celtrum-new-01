

<?php

return [

    //
    'home1' => 'Estimado usuario de Celtrum, ',
    'home2' => 'A causa de algunas actualizaciones, nuestro sitio web no está disponible en el momento, sin embargo el acceso a su dashboard permanece normal. ',
    'home3' => 'Le damos las gracias por su comprensión.',
    'home4' => 'Equipo de Celtrum',
    'home5' => 'Iniciar sesión',
    'home6' => 'Correo electrónico',
    'home7' => 'Contraseña',
    'home8' => 'Inicia sesión',
    

    // LANG
	'lang_img' => '/assets/flags/es.png',
	'lang' => 'ES',
    'lang-flag' => 'flag-icon flag-icon-es',
    




];


