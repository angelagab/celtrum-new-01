

<?php

return [

     //
     'home1' => 'Caro usuário Celtrum, ',
     'home2' => 'Devido a algumas atualizações nosso site se encontra indisponível no momento, porém, o acesso ao seu Dashboard continua normal. ',
     'home3' => 'Desde já, agradecemos sua compreensão.',
     'home4' => 'Equipe Celtrum',
     'home5' => 'Faça seu login',
     'home6' => 'Email',
     'home7' => 'Senha',
     'home8' => 'Entrar',

    // LANG
    'lang_img' => '/assets/flags/pt_br.png',
	'lang' => 'PT',
    'lang-flag' => 'flag-icon flag-icon-pt',



];
